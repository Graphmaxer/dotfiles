source /usr/share/zsh/share/antigen.zsh

antigen use oh-my-zsh

antigen bundle git
antigen bundle zsh-users/zsh-autosuggestions

antigen bundle zsh-users/zsh-syntax-highlighting

antigen bundle "greymd/docker-zsh-completion"

antigen theme robbyrussell

antigen apply

alias ne="emacs -nw"
alias sudo='sudo '
export VISUAL="emacs -nw"
export EDITOR="emacs -nw"
export PAGER="most"
export BAT_PAGER="less"
export PATH=$HOME/.gem/ruby/2.6.0/bin:$HOME/.yarn/bin:$HOME/.local/bin:$PATH
